extern crate linux;
extern crate mio;
extern crate walkdir;
extern crate futures;

use mio::*;
use linux::*;
use walkdir::WalkDir;
use futures::{Future, Async};

pub fn retain_mut<T, F>(vec: &mut Vec<T>, mut f: F)
    where F: FnMut(&mut T) -> bool
{
    let len = vec.len();
    let mut del = 0;
    {
        let v = &mut **vec;

        for i in 0..len {
            if !f(&mut v[i]) {
                del += 1;
            } else if del > 0 {
                v.swap(i - del, i);
            }
        }
    }
    if del > 0 {
        vec.truncate(len - del);
    }
}

fn retain<T, F>(vec: &mut Vec<T>, mut f: F) 
    where F: FnMut(&T) -> bool
{
    retain_mut(vec, |e| f(e))
}

#[test]
fn walkdir() {
    let files = WalkDir::new("/home/sebk/")
    .max_depth(100)
    .follow_links(true)
    .into_iter()
    .filter_map(|e| e.ok())
    .filter(|e| e.file_type().is_file())
    .inspect(|e| println!("{:?}", e.path()))
    .count();
}

#[test]
fn mio() {
    const DISK_IO: Token = Token(0);
    
    let poll = Poll::new().unwrap();
    let mut batch = Batch::new(100);
    poll.register(&batch, DISK_IO, Ready::readable(), PollOpt::edge()).unwrap();
    
    let mut events = Events::with_capacity(1024);
    
    let mut files = WalkDir::new("/home/sebk/")
    .max_depth(100)
    .follow_links(true)
    .into_iter()
    .filter_map(|e| e.ok())
    .filter(|e| e.file_type().is_file())
    .inspect(|e| println!("{:?}", e.path()))
    .filter_map(|e| File::open(e.path()).ok())
    .take(10000);
    
    let mut futures = Vec::with_capacity(100);
    let mut total = 0;
    
    loop {
        futures.extend(
            files.by_ref()
            .take(batch.free())
            .map(|f| batch.read(f, 0, 1024) )
        );
        
        batch.submit();
        
        if batch.pending() == 0 {
            assert!(batch.free() > 0);
            assert_eq!(0, futures.len());
            break;
        }
    
        /*
        poll.poll(&mut events, None).unwrap();

        for event in events.iter() {
            match event.token() {
                DISK_IO => {
                    let n = batch.poll();
                    println!("{} completed", n);
                }
                _ => unreachable!(),
            }
        }
        */
        
        println!("pending: {}", batch.pending());
        
        let n = batch.poll();
        println!("{} completed", n);
        
        retain_mut(&mut futures, |mut f| {
            match f.poll() {
                Ok(Async::Ready(buf)) => {
                    //println!("{} bytes ready", buf.len());
                    total += 1;
                    false
                }
                Err(e) => {
                    println!("{:?}", e);
                    false
                }
                _ => true
            }
        });
        println!("memory pool: {}", batch.free());
    }
    
    println!("{} files read", total);
}

