extern crate linux;
extern crate futures;
extern crate walkdir;

use linux::*;
use futures::Future;
use walkdir::WalkDir;

#[test]
fn open_file() {
    println!("{:?}", File::open("src/lib.rs"));
}

#[test]
fn async_read() {

    let mut b = Batch::new(100);
    let mut futures: Vec<_> = WalkDir::new("src").into_iter()
    .filter_map(|e| e.ok()).filter(|e| e.file_type().is_file())
    .map(|entry| {
        println!("open: {:?}", entry.path());
        let f = File::open(entry.path()).unwrap();
        b.read(f, 0, 1024)
    }).collect();
    
    b.submit();
    
    println!("still alive");
    b.poll();
    for f in &mut futures {
        println!("poll() -> {:?}", f.poll());
    }
}
