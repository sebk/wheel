#![feature(box_syntax)]
#![feature(nonzero)]
#![feature(const_fn)]

/*!
Linux syscalls:

 - `int io_setup(unsigned nr_events, aio_context_t *ctx_idp);`
 - `int io_destroy(aio_context_t ctx_id);`
 - `int io_submit(aio_context_t ctx_id, long nr, struct iocb **iocbpp);`
 - `int io_cancel(aio_context_t ctx_id, struct iocb *iocb, struct io_event *result);`
 - `int io_getevents(aio_context_t ctx_id, long min_nr, long nr,
    struct io_event *events, struct timespec *timeout);`
*/

#[macro_use] extern crate lazy_static;

extern crate syscall_alt;
extern crate libc;
extern crate futures;
extern crate mio;
extern crate common;
extern crate core;

pub mod fs;
pub mod io;
pub mod mem;
mod ffi;
mod eventfd;
mod util;
mod hub;
mod log;

pub use fs::*;
pub use io::*;
pub use mem::*;
pub use hub::*;
pub use log::*;


