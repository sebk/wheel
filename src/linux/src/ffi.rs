#![allow(dead_code)]

#[allow(non_camel_case_types)]
pub type aio_context_t = u64;

pub const IOCB_CMD_PREAD: u16 = 0;
pub const IOCB_CMD_PWRITE: u16 = 1;
pub const IOCB_CMD_FSYNC: u16 = 2;
pub const IOCB_CMD_FDSYNC: u16 = 3;
	/* These two are experimental.
	 * IOCB_CMD_PREADX = 4,
	 * IOCB_CMD_POLL = 5,
	 */
pub const IOCB_CMD_NOOP: u16 = 6;
pub const IOCB_CMD_PREADV: u16 = 7;
pub const IOCB_CMD_PWRITEV: u16 = 8;

pub const IOCB_FLAG_RESFD: u32 = 1 << 0;

#[repr(C)]
#[derive(Default, Debug)]
pub struct Iocb {
	/* these are internal to the kernel/libc. */
pub	aio_data:          u64,	   /* data to be returned in event's data */
pub	aio_key:           u32,    /* the kernel sets aio_key to the req # */
pub	aio_reserved1:     u32,
	
	/* common fields */
pub	aio_lio_opcode:    u16,    /* see IOCB_CMD_ above */
pub	aio_reqprio:       i16,
pub	aio_fildes:        u32,

pub	aio_buf:           u64,
pub	aio_nbytes:        u64,
pub	aio_offset:        i64,

	/* extra parameters */
pub	aio_reserved2:     u64,    /* TODO: use this for a (struct sigevent *) */

	/* flags for the "struct iocb" */
pub	aio_flags:         u32,

	/*
	 * if the IOCB_FLAG_RESFD flag of "aio_flags" is set, this is an
	 * eventfd to signal AIO readiness to
	 */
pub	aio_resfd:         u32,
}   /* 64 bytes */

#[derive(Debug)]
pub struct Event {
pub	data:  u64,		/* the data field from the iocb */
pub	obj:   u64,		/* what iocb this event came from */
pub	res:   i64,		/* result code for this event */
pub	res2:  i64		/* secondary result */
}
