extern crate rand;
extern crate linux;
extern crate futures;

use std::time::Instant;

use rand::Rng;
use linux::*;
use futures::{Future, Async};

pub fn retain_mut<T, F>(vec: &mut Vec<T>, mut f: F)
    where F: FnMut(&mut T) -> bool
{
    let len = vec.len();
    let mut del = 0;
    {
        let v = &mut **vec;

        for i in 0..len {
            if !f(&mut v[i]) {
                del += 1;
            } else if del > 0 {
                v.swap(i - del, i);
            }
        }
    }
    if del > 0 {
        vec.truncate(len - del);
    }
}

const MASK: u64 = !((1024 * 4) - 1);

fn main() {
    let mut rng = rand::thread_rng();

    let mut batch = Batch::new(1024 * 16, 100);
    let mut futures = Vec::with_capacity(100);
    let mut file = SharedFile::open("/dev/sda1").expect("failed to open device");
    
    loop {
        for i in 0 .. 10 {
            let pos = (rng.gen::<u32>() as u64 & MASK);
            futures.push(batch.read(file.clone(), pos, 1024 * 16))
        }
        
        let t0 = Instant::now();
        
        batch.submit();
        
        let t1 = Instant::now();
        
        while batch.pending() > 0 {
            let n = batch.poll();
            
            let t2 = Instant::now();
            
            println!("{} completed in {:?}", n, t2.duration_since(t0));
            
            retain_mut(&mut futures, |mut f| {
                match f.poll() {
                    Ok(Async::Ready(buf)) => {
                        false
                    }
                    Err(e) => {
                        false
                    }
                    _ => true
                }
            });
        }
        println!("submit: {:?}", t1.duration_since(t0));
    }
}
