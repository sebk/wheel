use libc;

thread_local! {
    static ERRNO: *const i32 = unsafe { libc::__errno_location() };
}

lazy_static! {
    pub static ref PAGE_SIZE: usize = unsafe { libc::sysconf(libc::_SC_PAGE_SIZE) as usize };
}

// FIXME
//pub const HUGEPAGE_SIZE: usize      = 2 * 1024 * 1024;
pub const BLOCK_SIZE: usize         = 4096;
pub const READ_ALIGN_MASK: usize    = (BLOCK_SIZE - 1);

#[inline(always)]
pub fn errno(ret: isize) -> i32 {
    if ret == -1 {
        ERRNO.with(|&e| unsafe { *e })
    } else {
        -ret as i32
    }
}
