use eventfd::EventFd;
use std::os::unix::io::AsRawFd;
use std::rc::Rc;
use std::cell::RefCell;
use std::mem::swap;
use std::cmp::min;
use futures::{Future, Stream, Poll, Async};
use io::*;
use mem::*;
use util::*;
use mio;
use ffi;
use libc;

#[derive(Debug)]
enum State {
    Queued,
    Running(Slice<u8>),
    Complete(Slice<u8>),
    Failed(ReadError),
    Taken
}

#[derive(Debug)]
struct EntryInner {
    iocb:   ffi::Iocb,
    state:  State,
    size:   usize
}
type QueueEntry = Rc<RefCell<EntryInner>>;
type EntryCell = RefCell<EntryInner>;

#[derive(Debug)]
pub struct Read<F: AsRawFd> {
    inner:  QueueEntry,
    file:   F
}
impl<F: AsRawFd> Future for Read<F> {
    type Item = Slice<u8>;
    type Error = ReadError;
    
    fn poll(&mut self) -> Poll<Slice<u8>, ReadError> {
        let entry = &mut *self.inner.borrow_mut();
        let mut tmp = State::Taken;
        swap(&mut tmp, &mut entry.state);
        
        match tmp {
            State::Running(buffer) => {
                entry.state = State::Running(buffer);
                Ok(Async::NotReady)
            },
            State::Complete(buffer) => Ok(Async::Ready(buffer)),
            State::Failed(error) => Err(error),
            State::Queued => Err(ReadError::NotSubmitted),
            State::Taken => Err(ReadError::Taken)
        }
    }
}

struct HubInner {
    ctx:        Option<AIoContext>,
    eventfd:    EventFd,
    pool:       MemoryPool,
    queue:      Vec<QueueEntry>,
    pending:    usize,
    pool_size:  usize
}
pub struct Hub {
    inner:      RefCell<HubInner>
}

#[derive(Clone)]
pub struct HubRef {
    inner:      Rc<Hub>
}

thread_local! {
    pub static HUB_REF: HubRef = HubRef {
        inner: Rc::new(Hub::new(BLOCK_SIZE, 16))
    };
}

impl HubInner {
    pub fn new(item_size: usize, capacity: usize) -> HubInner {
        HubInner {
            eventfd:    EventFd::new(0, libc::EFD_NONBLOCK).unwrap(),
            ctx:        None,
            pool:       MemoryPool::new(item_size, capacity).expect("failed to create MemoryPool"),
            queue:      Vec::new(),
            pending:    0,
            pool_size:  item_size
        }
    }
    
    pub fn free(&self) -> usize {
        self.pool.free()
    }
    
    pub fn read<F: AsRawFd>(&mut self, file: F, offset: u64, size: usize)
     -> Read<F>
    {
        let total_size = (size + BLOCK_SIZE - 1) & !READ_ALIGN_MASK;
        println!("read({}, offset={}, size={})", file.as_raw_fd(), offset, total_size);
        assert_eq!(offset as usize & READ_ALIGN_MASK, 0);
        assert_eq!(total_size & READ_ALIGN_MASK, 0);
        let entry = Rc::new(RefCell::new(EntryInner {
            iocb:   ffi::Iocb {
                aio_lio_opcode: ffi::IOCB_CMD_PREAD,
                aio_fildes:     file.as_raw_fd() as u32,
                aio_nbytes:     size as u64,
                aio_offset:     offset as i64,
                ..              ffi::Iocb::default()
            },
            state:  State::Queued,
            size:   size
        }));
        self.queue.push(entry.clone());
        self.submit();
        Read { inner: entry, file: file }
    }

    pub fn submit(&mut self) {
        let mut iocbs = Vec::with_capacity(self.queue.len());
        
        while let Some(item) = self.queue.pop() {
            let i_ptr: *const EntryCell = Rc::into_raw(item);
            let i = unsafe { &*i_ptr }; // we are not ready yet, but need the poiner
            let e: &mut EntryInner = &mut *i.borrow_mut();
            let buffer = {
                let ref mut iocb = e.iocb;
                
                // get a buffer
                let mut buffer = if self.pool_size >= iocb.aio_nbytes as usize {
                    match self.pool.get() {
                        Some(buffer) => buffer,
                        _ => Slice::with_capacity(iocb.aio_nbytes as usize)
                    }
                } else {
                    Slice::with_capacity(iocb.aio_nbytes as usize)
                };
                assert!(buffer.capacity() >= iocb.aio_nbytes as usize);
                
                iocb.aio_buf    = buffer.as_mut_ptr() as *mut u8 as usize as u64;
                iocb.aio_data   = i_ptr as u64; // in particular this field.
                iocb.aio_resfd  = self.eventfd.as_raw_fd() as u32;
                iocb.aio_flags  = ffi::IOCB_FLAG_RESFD;
                
                buffer
            };
            e.state = State::Running(buffer);
            
            iocbs.push(&e.iocb as *const ffi::Iocb);
        }
        
        let n = iocbs.len();
        if n == 0 {
            return;
        }
        
        let ctx = match self.ctx.take() {
            Some(ctx) => {
                if ctx.capacity() < n {
                    ctx.destroy();
                    AIoContext::setup(n.next_power_of_two()).unwrap()
                } else {
                    ctx
                }
            },
            None => AIoContext::setup(n.next_power_of_two()).unwrap()
        };
        unsafe {
            ctx.submit(&iocbs).expect("submit failed");
        }
        self.ctx = Some(ctx);
        self.pending += n;
    }
    
    pub fn poll(&mut self) -> usize {
        let ref ctx = self.ctx.as_ref().expect("not initialized");
        
        println!("try read eventfd");
        let n_bytes = self.eventfd.read().unwrap();
        println!("eventfd.read() -> {}", n_bytes);
        
        let mut events = Vec::with_capacity(self.pending);
        
        ctx.get_events(1, &mut events, None).unwrap();
        let n = events.len();
        
        for event in events {
            let item = unsafe {
                Rc::from_raw(event.data as *mut EntryCell)
            };
            
            let mut entry = item.borrow_mut();
            let mut tmp = State::Taken;
            swap(&mut tmp, &mut entry.state);
            
            entry.state = match tmp {
                State::Running(mut buffer) => {
                    match entry.iocb.aio_lio_opcode {
                        ffi::IOCB_CMD_PREAD => {
                            if event.res >= 0 {
                                println!("{}/{}", event.res, entry.size);
                                unsafe {
                                    buffer.set_len(min(event.res as usize, entry.size));
                                }
                                State::Complete(buffer)
                            } else {
                                State::Failed(ReadError::Errno(-event.res as i32))
                            }
                        },
                        o => panic!("invalid opcode {}", o)
                    }
                },
                s => panic!("invalid state {:?}", s)
            };
        }
        self.pending -= n;
        n
    }
    
    pub fn pending(&self) -> usize {
        self.pending
    }
}

impl Drop for HubInner {
    fn drop(&mut self) {
        if let Some(ctx) = self.ctx.take() {
            ctx.destroy();
        }
    }
}

impl Hub {
    pub fn new(item_size: usize, capacity: usize) -> Hub {
        Hub {
            inner: RefCell::new(HubInner::new(item_size, capacity))
        }
    }
    pub fn free(&self) -> usize {
        self.inner.borrow().free()
    }
    pub fn read<F: AsRawFd>(&self, file: F, offset: u64, size: usize) -> Read<F> {
        self.inner.borrow_mut().read(file, offset, size)
    }
    pub fn execute_future(&self, mut f: Box<Future<Item=(), Error=()>>) {
        println!("execute_future: entering loop");
        loop {
            println!("  polling");
            match f.poll() {
                Ok(Async::Ready(_)) | Err(_) => break,
                Ok(Async::NotReady) => { self.inner.borrow_mut().poll(); }
            }
        }
    }
    pub fn execute_stream(&self, mut s: Box<Stream<Item=(), Error=()>>) {
        println!("execute_stream: entering loop");
        loop {
            self.inner.borrow_mut().submit();
            println!("  polling");
            match s.poll() {
                Ok(Async::Ready(None)) => break,
                Ok(Async::Ready(Some(_))) | Err(_) => continue,
                Ok(Async::NotReady) => {
                    self.inner.borrow_mut().poll();
                }
            }
        }
    }
}

impl HubRef {
    pub fn cloned() -> HubRef {
        HUB_REF.with(|hub| hub.clone())
    }
    pub fn local<F, O>(f: F) -> O where F: FnOnce(&Hub) -> O {
        HUB_REF.with(|hub| f(&*hub.inner))
    }
}
impl mio::Evented for Hub {
    fn register(&self,
        poll: &mio::Poll, token: mio::Token,
        interest: mio::Ready, opts: mio::PollOpt)
     -> ::std::io::Result<()>
    {
        mio::unix::EventedFd(&self.inner.borrow().eventfd.as_raw_fd())
        .register(poll, token, interest, opts)
    }

    fn reregister(&self,
        poll: &mio::Poll, token: mio::Token, 
        interest: mio::Ready, opts: mio::PollOpt)
     -> ::std::io::Result<()>
    {
        mio::unix::EventedFd(&self.inner.borrow().eventfd.as_raw_fd())
        .reregister(poll, token, interest, opts)
    }

    fn deregister(&self, poll: &mio::Poll) -> ::std::io::Result<()> {
        mio::unix::EventedFd(&self.inner.borrow().eventfd.as_raw_fd())
        .deregister(poll)
    }
}

