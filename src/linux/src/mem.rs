use std::rc::Rc;
use std::ops::{Deref, DerefMut};
use std::cell::RefCell;
use std::{fmt, cmp, mem, ptr, slice, usize};
use libc;
use syscall_alt::syscalls::Syscall;
use util::*;

#[derive(Debug)]
pub enum MMapError {
    InvalidSize,
    OutOfMemory
}

/* This may only be used inside MemoryPool.
   It has to be converted to a Slice to hand it to to the user.
   We do not keep a Rc to avoid cyclic references . */
struct FreeSlice {
    ptr:    *mut u8,    // owned data of the slice
    size:   usize,      // size of the slice
}

struct PoolInner {
    free:   Vec<FreeSlice>,
    region: Region,
    size:   usize
}
impl PoolInner {
    fn new(size: usize, capacity: usize) -> Result<PoolInner, MMapError> {
        let total_size = cmp::max(*PAGE_SIZE, (size * capacity).next_power_of_two());
        let huge = false; //total_size >= HUGEPAGE_SIZE;
        
        println!("allocating {} bytes", total_size);
        let mut p = PoolInner {
            free:   Vec::with_capacity(capacity),
            region: Region::new(total_size, huge)?,
            size:   size
        };
        let mut ptr = p.region.addr;
        while ptr < p.region.addr + total_size {
            p.free.push(FreeSlice {
                ptr:    ptr as *mut u8,
                size:   size
            });
            ptr += size;
        }
        Ok(p)
    }
    fn push(&mut self, f: FreeSlice) {
        self.free.push(f)
    }
    fn get(&mut self) -> Option<FreeSlice> {
        self.free.pop()
    }
    fn free(&self) -> usize {
        self.free.len()
    }
}

pub struct Slice<T> {
    /* keeping an Rc to the MemoryPool ensures the data is valid */
    ptr:        *mut T,
    valid:      usize, // in size_of::<T>()
    capacity:   usize, // ^
    owner:      Option<Rc<RefCell<PoolInner>>>
}
impl<T> Slice<T> {
    pub fn with_capacity(cap: usize) -> Slice<T> {
        let mut v = Vec::with_capacity(cap);
        let s = Slice {
            ptr:        v.as_mut_ptr(),
            valid:      v.len(),
            capacity:   v.capacity(),
            owner:      None
        };
        mem::forget(v);
        
        s
    }
    #[inline(always)]
    pub fn capacity(&self) -> usize {
        if mem::size_of::<T>() > 0 {
            self.capacity
        } else {
            usize::MAX
        }
    }
    #[inline(always)]
    pub fn len(&self) -> usize {
        self.valid
    }
    #[inline(always)]
    pub fn push(&mut self, value: T) {
        assert!(self.valid + 1 <= self.capacity, "Slice is full");
        unsafe {
            ptr::write(self.ptr.offset(self.valid as isize), value);
        }
        self.valid += 1;
    }
    #[inline(always)]
    pub fn pop(&mut self) -> Option<T> {
        if self.valid > 0 {
            self.valid -= 1;
            Some(unsafe { ptr::read(self.ptr.offset(self.valid as isize)) })
        } else {
            None
        }
    }
    pub unsafe fn set_len(&mut self, len: usize) {
        assert!(len <= self.capacity);
        self.valid = len;
    }
    pub fn to_vec(self) -> Vec<T> {
        let v = if let Some(ref pool) = self.owner {
            let mut vec = Vec::with_capacity(self.valid);
            unsafe {
                ptr::copy_nonoverlapping(self.ptr, vec.as_mut_ptr(), self.valid);
                vec.set_len(self.valid);
            }
            
            pool.borrow_mut().push(
                FreeSlice {
                    ptr:    self.ptr as *mut u8,
                    size:   self.capacity * mem::size_of::<T>()
                }
            );
            
            vec
        } else {
            unsafe {
                Vec::from_raw_parts(self.ptr, self.valid, self.capacity)
            }
        };
        
        // must not run drop
        mem::forget(self);
        
        v
    }
}

impl<T> Slice<T> where T: Copy {
    pub fn set(&mut self, n: usize, value: T) {
        assert!(self.valid + n <= self.capacity, "Slice is full");
        for i in self.valid .. self.valid + n {
            unsafe {
                ptr::write(self.ptr.offset(i as isize), value);
            }
        }
        self.valid += n;
    }
}

impl<T> Deref for Slice<T> {
    type Target = [T];
    #[inline(always)]
    fn deref(&self) -> &[T] {
        unsafe {
            /* we own a reference to the memory pool, which owns the data */
            slice::from_raw_parts(self.ptr, self.valid)
        }
    }
}
impl<T> DerefMut for Slice<T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut [T] {
        unsafe {
            /* we own a reference to the memory pool, which owns the data */
            slice::from_raw_parts_mut(self.ptr, self.valid)
        }
    }
}
impl<T> Drop for Slice<T> {
    fn drop(&mut self) {
        if mem::size_of::<T>() > 0 {
            assert!(self.capacity > 0);
            for i in 0 .. self.valid {
                unsafe {
                    ptr::drop_in_place(self.ptr.offset(i as isize));
                }
            }
            if let Some(ref pool) = self.owner {
                pool.borrow_mut().push(
                    FreeSlice {
                        ptr:    self.ptr as *mut u8,
                        size:   self.capacity * mem::size_of::<T>()
                    }
                );
                self.capacity = 0;
            } else {
                unsafe {
                    Vec::from_raw_parts(self.ptr, self.valid, self.capacity);
                }
            }
        } else {
            for _ in 0 .. self.valid {
                unsafe {
                    ptr::drop_in_place(1 as *mut T);
                }
            }
        }
    }
}
impl<T: fmt::Debug> fmt::Debug for Slice<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        if self.valid > 20 {
            write!(f, "Slice {:?} .. ({} total)", &self[.. 20], self.valid)
        } else {
            write!(f, "Slice {:?}", &self[.. self.valid])
        }
    }
}
impl<T> Into<Vec<T>> for Slice<T> {
    fn into(self) -> Vec<T> {
        self.to_vec()
    }
}

pub struct MemoryPool {
    inner:  Rc<RefCell<PoolInner>>
}
impl MemoryPool {
    pub fn new(size: usize, capacity: usize) -> Result<MemoryPool, MMapError> {
        assert!(size > 0);
        assert!(capacity > 0);
        assert_eq!(size % BLOCK_SIZE, 0);
        Ok(MemoryPool {
            inner:  Rc::new(RefCell::new(PoolInner::new(size, capacity)?))
        })
    }
    pub fn get<T>(&self) -> Option<Slice<T>> {
        if mem::size_of::<T>() > 0 {
            if let Some(f) = self.inner.borrow_mut().get() {
                assert_eq!(f.ptr as usize % mem::align_of::<T>(), 0, 
                    "type alignment exeeds MemoryPool granularity");
                    
                Some(Slice {
                    ptr:        f.ptr as *mut T,
                    capacity:   f.size / mem::size_of::<T>(),
                    valid:      0,
                    owner:      Some(self.inner.clone())
                })
            } else {
                None
            }
        } else {
            Some(Slice {
                ptr:        1 as *mut T,
                capacity:   0,
                valid:      0,
                owner:      None
            })
        }
    }
    pub fn free(&self) -> usize {
        self.inner.borrow().free()
    }
}

/* allocated by mmap */
pub struct Region {
    addr:   usize,
    size:   usize
}
impl Region {
    fn new(size: usize, huge: bool) -> Result<Region, MMapError> {
        let flags = libc::MAP_ANONYMOUS | libc::MAP_PRIVATE | (huge as i32 * libc::MAP_HUGETLB);
        let res = unsafe {
            Syscall::mmap.syscall6(
                0,
                size as isize,
                (libc::PROT_READ | libc::PROT_WRITE) as isize,
                flags as isize,
                -1,
                0
            )
        };
        if res >= 0 {
            Ok(Region {
                addr:   res as usize,
                size:   size
            })
        } else {
            match errno(res) {
                libc::EINVAL => Err(MMapError::InvalidSize),
                libc::ENOMEM => Err(MMapError::OutOfMemory),
                e => panic!("mmap: result = {}, errno = {}", res, e)
            }
        }
    }
}
impl Drop for Region {
    fn drop(&mut self) {
        let res = unsafe {
            Syscall::munmap.syscall2(self.addr as isize, self.size as isize)
        };
        if res != 0 {
            panic!("munmap failed");
        }
    }
}
impl Deref for Region {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        unsafe {
            /* we own a reference to the memory pool, which owns the data */
            slice::from_raw_parts(self.addr as *const u8, self.size)
        }
    }
}
impl DerefMut for Region {
    fn deref_mut(&mut self) -> &mut [u8] {
        unsafe {
            /* we own a reference to the memory pool, which owns the data */
            slice::from_raw_parts_mut(self.addr as *mut u8, self.size)
        }
    }
}
