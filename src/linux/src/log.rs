use common::LogLevel;
use core::nonzero::NonZero;
use core::fmt::Arguments;
use std::io::{Write, stderr};
use std::sync::atomic::{AtomicUsize, Ordering};

static LOG_ID: AtomicUsize = AtomicUsize::new(3);
static LOG_ROOT: LogId = unsafe { NonZero::new_unchecked(2) };

pub type LogId = NonZero<usize>;

#[derive(Copy, Clone)]
pub struct Log {
    id: Option<LogId>
}
impl Log {
    pub fn root() -> Log {
        Log { id: Some(LOG_ROOT) }
    }
    pub fn branch(&self) -> Log {
        Log {
            id: self.id.map(|_id| {
                let child = LOG_ID.fetch_add(1, Ordering::Relaxed);
                unsafe { LogId::new_unchecked(child) }
            })
        }
    }
    pub fn log(&self, level: LogLevel, args: Arguments) {
        if let Some(_id) = self.id {
            let mut e = stderr();
            e.write(match level {
                LogLevel::Warn => "\x1b[33m",
                _ => ""
            }.as_bytes()).unwrap();
            e.write_fmt(args).unwrap();
            e.write(b"\x1b[0m\n").unwrap();
        }
    }
}
