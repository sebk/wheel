use core::cell::RefCell;
use core::fmt;
use alloc::boxed::Box;
use futures::{Future, Stream, Async};

pub struct Hub {
    current:    RefCell<Execution>
}
// JavaScript has no threads, therefore it is threadsafe.
unsafe impl Sync for Hub {}

static HUB_INNER: Hub = Hub {
    current:    RefCell::new(Execution::Stopped)
};

#[derive(Clone)]
pub struct HubRef {
    inner: &'static Hub
}
// JavaScript has no threads, therefore it is threadsafe.
unsafe impl Sync for HubRef {}

// item = () crashes llc
type HubRefFuture = Box<Future<Item=(), Error=()>>;
type HubRefStream = Box<Stream<Item=(), Error=()>>;
enum Execution {
    Running,
    Future(HubRefFuture),
    Stream(HubRefStream),
    Stopped
}
impl fmt::Debug for Execution {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Execution::Running => write!(f, "Running"),
            &Execution::Future(_) => write!(f, "Future"),
            &Execution::Stream(_) => write!(f, "Stream"),
            &Execution::Stopped => write!(f, "Stopped")
        }
    }
}

impl HubRef {
    pub fn execute_future(&self, f: HubRefFuture) {
        let mut current = self.inner.current.borrow_mut();
        match *current {
            Execution::Running => (),
            Execution::Stopped => (),
            ref e => panic!("execute_future: invalid state {:?}", e)
        }
        
        *current = Execution::Future(f);
    }
    pub fn execute_stream(&self, s: HubRefStream) {
        let mut current = self.inner.current.borrow_mut();
        match *current {
            Execution::Running => (),
            Execution::Stopped => (),
            ref e => panic!("execute_stream: invalid state {:?}", e)
        }
        
        *current = Execution::Stream(s);
    }
    pub fn resume(&self) {
        use core::mem::replace;
        
        let old = replace(&mut *self.inner.current.borrow_mut(), Execution::Running);
        let new = match old {
            Execution::Future(mut future) => match future.poll() {
                Ok(Async::NotReady) => Execution::Future(future),
                Ok(Async::Ready(_)) => Execution::Stopped,
                Err(()) => Execution::Stopped
            },
            Execution::Stream(mut stream) => match stream.poll() {
                Ok(Async::NotReady) => Execution::Stream(stream),
                Ok(Async::Ready(Some(_))) => Execution::Stream(stream),
                Ok(Async::Ready(None)) => Execution::Stopped,
                Err(()) => Execution::Stream(stream)
            },
            Execution::Stopped => Execution::Stopped,
            Execution::Running => return
        };
        *self.inner.current.borrow_mut() = new;
    }
    
    pub fn cloned() -> Self {
        HubRef { inner: &HUB_INNER }
    }
    
    pub fn local<F, O>(f: F) -> O where F: FnOnce(&HubRef) -> O {
        f(&HubRef { inner: &HUB_INNER })
    }
}

