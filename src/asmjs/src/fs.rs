use alloc::boxed::Box;
use collections::Vec;
use futures::Future;
use common::{AsyncDirectory, AsyncRead, AsyncWrite, AsyncOpen};
use task::TaskError;
use ffi;

#[derive(Clone, Debug)]
pub struct Directory {
    handle: u32
}
impl Directory {
    pub fn from_handle(h: u32) -> Directory {
        Directory { handle: h }
    }
}
impl AsyncDirectory for Directory {
    type File = File;
    type Error = TaskError;
    
    fn get_directory(&self, name: &str) -> Box<Future<Item=Self, Error=Self::Error>> {
        box ffi::dir_get_directory(self.handle, name)
            .map(|(_, h)| Directory::from_handle(h))
    }
    fn get_file(&self, name: &str) -> Box<Future<Item=Self::File, Error=Self::Error>> {
        box ffi::dir_get_file(self.handle, name)
            .map(|(_, h)| File::from_handle(h))
    }
}
impl AsyncOpen for Directory {
    type Error = TaskError;
    
    fn open(name: &str) -> Box<Future<Item=Self, Error=Self::Error>> {
        box ffi::async_open_directory(name).map(|(_, h)| Directory::from_handle(h))
    }
}
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct File {
    handle: u32
}
impl File {
    pub fn from_handle(h: u32) -> File {
        File { handle: h }
    }
    
    pub fn handle(&self) -> u32 {
        self.handle
    }
}
impl AsyncRead for File {
    type Error = TaskError;
    type Buffer = Vec<u8>;
    
    fn read(&self) -> Box<Future<Item=Self::Buffer, Error=Self::Error>>
    {
        box ffi::read(self.handle).map(|(_, data)| data)
    }
}
impl AsyncWrite for File {
    type Error = TaskError;
    type Buffer = Vec<u8>;
    
    fn write(&self, data: Self::Buffer) -> Box<Future<Item=Self::Buffer, Error=Self::Error>>
    {
        box ffi::write(self.handle, data).map(|(buf, _)| buf)
    }
}
