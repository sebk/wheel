use collections::{Vec, String};
use core::cell::RefCell;
use core::mem::replace;
use core::marker::PhantomData;
use alloc::rc::Rc;
use futures::{Poll, Async, Stream, Sink, AsyncSink, StartSend};
use ffi;
use image::RgbaImage;
use serde_cbor;
use serde::{Serialize, Deserialize};

pub enum PortRx {
    Pending,
    Failed(String),
    Ready(Vec<u8>)
}

pub type PortRxCell = RefCell<PortRx>;

pub struct Port<Rx, Tx> {
    handle: u32,
    rx:     Rc<PortRxCell>,
    _m:     PhantomData<(Rx, Tx)>
}

impl<Rx, Tx> Port<Rx, Tx> where Rx: Deserialize, Tx: Serialize {
    pub fn from_handle(handle: u32) -> Port<Rx, Tx> {
        let rx = Rc::new(RefCell::new(PortRx::Pending));
        ffi::port_start(handle, box Rc::downgrade(&rx));
            
        Port {
            handle: handle,
            rx:     rx,
            _m:     PhantomData
        }
    }
    pub fn send(&self, data: &Tx) {
        let v = serde_cbor::to_vec(data).expect("failed to serialize");
        ffi::port_send(self.handle, &v);
    }
}

impl<Rx, Tx> Stream for Port<Rx, Tx> where Rx: Deserialize, Tx: Serialize {
    type Item = Rx;
    type Error = String;
    
    fn poll(&mut self) -> Poll<Option<Rx>, Self::Error> {
        let rx = &mut *self.rx.borrow_mut();
        match replace(rx, PortRx::Pending) {
            PortRx::Pending => {
                if let Some(data) = ffi::port_poll(self.handle) {
                    match serde_cbor::from_slice(&data) {
                        Ok(value) => Ok(Async::Ready(Some(value))),
                        Err(e) => Err(format!("{:?}", e))
                    }
                } else {
                    Ok(Async::NotReady)
                }
            },
            PortRx::Ready(data) => match serde_cbor::from_slice(&data) {
                Ok(value) => Ok(Async::Ready(Some(value))),
                Err(e) => Err(format!("{:?}", e))
            },
            PortRx::Failed(msg) => Err(msg)
        }
    }
}
impl<Rx, Tx> Sink for Port<Rx, Tx> where Rx: Deserialize, Tx: Serialize
{
    type SinkItem = Tx;
    type SinkError = String;
    
    fn start_send(&mut self, item: Self::SinkItem)
     -> StartSend<Self::SinkItem, Self::SinkError>
    {
        Port::send(self, &item);
        Ok(AsyncSink::Ready)
    }
    
    fn poll_complete(&mut self) -> Poll<(), Self::SinkError> {
        Ok(Async::Ready(()))
    }
}


