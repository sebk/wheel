function hello() {
    return "Hello World";
}

function set_string(string_ptr, text) {
    var encoder = new TextEncoder();
    var text_utf8 = encoder.encode(text);
    
    var buf_ptr = Module._prepare_string(string_ptr, text_utf8.length);
    if (buf_ptr !== 0) {
        Module.HEAPU8.set(text_utf8, buf_ptr);
    }
}

function do_something(text) {
    var string = _create_string();
    set_string(string, text);
    _do_something(string);
}
