mergeInto(LibraryManager.library, {
    js_hello: function(string_ptr) {
        var msg_str = hello();
        var encoder = new TextEncoder();
        var msg_utf8 = encoder.encode(msg_str);
        
        var buf_ptr = Module._prepare_string(string_ptr, msg_utf8.length);
        if (buf_ptr !== 0) {
            Module.HEAPU8.set(msg_utf8, buf_ptr);
        }
    }
});
