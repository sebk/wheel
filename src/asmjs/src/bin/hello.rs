#![feature(link_args)]

#[cfg_attr(
    target_arch="asmjs",
    link_args="\
        -s EXPORTED_FUNCTIONS=[\
            '_prepare_string',\
            '_create_string',\
            '_do_something',\
            '_call_hello'\
        ] \
        --js-library src/bin/hello.lib.js \
")]
#[allow(improper_ctypes)]
extern {
    fn js_hello(out: *mut String);
}

fn hello() -> String {
    let mut out = String::new();
    unsafe {
        js_hello(&mut out as *mut String);
    }
    out
}


#[no_mangle]
pub extern fn prepare_string(string_ptr: *mut String, len: usize) -> *mut u8 {
    let string = unsafe { &mut *string_ptr };
    let cap = string.capacity();
    if cap < len {
        string.reserve(len - cap);
    }
    let p = unsafe {
        let v = string.as_mut_vec();
        v.set_len(len);
        v.as_mut_slice().as_mut_ptr()
    };
    println!("prepare_string({:p}) -> {:p}", string_ptr, p);
    p
}

#[no_mangle]
pub extern fn create_string() -> *mut String {
    let p = Box::into_raw(Box::new(String::new()));
    println!("create_string() -> {:p}", p);
    p
}

#[no_mangle]
pub extern fn do_something(s: *mut String) {
    println!("{:p}", s);
    let s = unsafe { Box::from_raw(s) };
    println!("do_something({})", s);
}

#[no_mangle]
pub extern fn call_hello() {
    println!("hello() -> {}", hello());
}

fn main() {
}
