use core::nonzero::NonZero;
use core::sync::atomic::{AtomicUsize, Ordering};
use core::fmt::{Arguments, Write};
use collections::String;

use common::LogLevel;
use ffi;

pub type LogId = NonZero<usize>;

static LOG_ID: AtomicUsize = AtomicUsize::new(2);
const LOG_ROOT: Log = unsafe {
    Log { id: Some(NonZero::new_unchecked(1)) }
};

fn log(l: LogId, level: LogLevel, args: Arguments) {
    let mut msg = String::new();
    let _ = msg.write_fmt(args);
    ffi::log(l.get(), level as usize, &msg);
}

fn log_branch(parent: LogId) -> LogId {
    let child = LOG_ID.fetch_add(1, Ordering::Relaxed);
    unsafe {
        ffi::log_branch(parent.get(), child);
        LogId::new(child).unwrap()
    }
}

#[derive(Copy, Clone)]
pub struct Log {
    id: Option<LogId>
}
impl Log {
    pub fn root() -> Log {
        LOG_ROOT
    }
    pub fn branch(&self) -> Log {
        Log { id: self.id.map(|id| log_branch(id)) }
    }
    pub fn log(&self, level: LogLevel, args: Arguments) {
        if let Some(id) = self.id {
            log(id, level, args)
        }
    }
}
