use collections::String;
use core::cell::RefCell;
use core::mem::replace;
use alloc::rc::{Rc, Weak};
use futures::{Future, Poll, Async};

#[derive(Debug)]
pub enum TaskError {
    Taken,
    Other(String)
}

pub enum Promise<D, R> {
    Pending(D),
    Failed(D, String),
    Complete(D, R),
    Taken
}

impl<D, R> Promise<D, R> {
    pub fn complete(&mut self, result: R) {
        match replace(self, Promise::Taken) {
            Promise::Pending(data) => {
                *self = Promise::Complete(data, result);
            },
            _ => panic!("Promise is not pending! this is a bug.")
        };
    }
    pub fn failed(&mut self, msg: String) {
        match replace(self, Promise::Taken) {
            Promise::Pending(data) => {
                *self = Promise::Failed(data, msg);
            },
            _ => panic!("Promise is not pending! this is a bug.")
        };
    }
}

pub type PromiseCell<D, R> = RefCell<Promise<D, R>>;

pub struct Task<D, R> {
    inner:  Rc<PromiseCell<D, R>>
}
impl<D, R> Task<D, R> {
    fn new(data: D) -> Task<D, R> {
        Task {
            inner: Rc::new(PromiseCell::new(Promise::Pending(data)))
        }
    }
    pub fn future(data: D) -> Task<D, R> {
        Task::new(data)
    }
    pub fn replace(&self, p: Promise<D, R>) -> Promise<D, R> {
        replace(&mut *self.inner.borrow_mut(), p)
    }
    pub fn downgrade(&self) -> Weak<PromiseCell<D, R>> {
        Rc::downgrade(&self.inner)
    }
}

    
impl<D, R> Future for Task<D, R> {
    type Item = (D, R);
    type Error = TaskError;
    
    fn poll(&mut self) -> Poll<(D, R), TaskError> {
        let mut promise = self.inner.borrow_mut();
        
        if let Promise::Pending(_) = *promise {
            Ok(Async::NotReady)
        } else {
            match replace(&mut *promise, Promise::Taken) {
                Promise::Complete(data, result) => {
                    Ok(Async::Ready((data, result)))
                }
                Promise::Failed(_data, msg) => {
                    Err(TaskError::Other(msg))
                }
                Promise::Taken => {
                    Err(TaskError::Taken)
                },
                Promise::Pending(_) => unreachable!()
            }
        }
    }
}

