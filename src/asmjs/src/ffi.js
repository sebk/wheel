mergeInto(LibraryManager.library, {
    // transfers ownership to JS
    _async_fetch: function (
        task,       // *mut DataTaskRef
        url_ptr,    // *const u8
        url_len     // usize
    ) {
        var task = DataTask(task);
        var url = utf8ToString(url_ptr, url_len);
        fetch(url).then(function (response) {
            return response.arrayBuffer();
        }).then(task.complete, task.failed);
    },
    
    _async_read: function (
        task,       // *mut DataTaskRef
        handle      // u32
    ) {
        var task = DataTask(task);
        HANDLES[handle].read()
        .then(task.complete, task.failed);
    },
    
    // transfers ownership to JS
    _async_write: function (
        task,       // *mut WriteTaskRef
        url_ptr,    // *const u8,
        url_len,    // usize,
        data_ptr,   // *const u8,
        data_len   // usize
    ) {
        var task = new WriteTask(task);
        var url = utf8ToString(url_ptr, url_len);
        var data = Module.HEAPU8.subarray(data_ptr, data_ptr+data_len);
        fetch(url, {
            method: 'POST',
            body:   data
        }).then(function (response) {
            return response.arrayBuffer();
        }).then(task.complete, task.failed);
    },
    
    _async_open_directory: function (
        task,       // *mut HandleTaskRef
        url_ptr,    // *const u8,
        url_len     // usize,
    ) {
        var task = HandleTask(task);
        open_directory(utf8ToString(url_ptr, url_len))
        .then(task.complete, task.failed);
    },
    
    _port_poll: function (
        handle,     // Handle,
        data        // *mut Data
    ) {             // -> bool
        return PORTS[handle].poll(data);
    },
    _port_start: function (
        handle,     // 32,
        rx          // *mut Weak<PortRxCell>
    ) {
        PORTS[handle].start(rx);
    },
    _port_send: function (
        handle,     // u32
        data_ptr,   // *const u8,
        data_len    // usize
    ) {
        var data = Module.HEAPU8.subarray(data_ptr, data_ptr+data_len);
        PORTS[handle].send(CBOR.decode(data));
    },
    _dir_get_file: function (
        task,       // HandleTaskRef
        handle,     // u32,
        name_ptr,   // *const u8,
        name_len    // usize
    ) {             //  -> u32;
        var task = HandleTask(task);
        var name = utf8ToString(name_ptr, name_len);
        HANDLES[handle].get_file(name)
        .then(task.complete, task.failed);
    },
    _dir_get_directory: function (
        task,       // HandleTaskRef
        handle,     // u32,
        name_ptr,   // *const u8,
        name_len    // usize
    ) {             //  -> u32;
        var task = HandleTask(task);
        var name = utf8ToString(name_ptr, name_len);
        HANDLES[handle].get_directory(name)
        .then(task.complete, task.failed);
    },
    _log: function (id, level, ptr, len) {
        log(id, level, utf8ToString(ptr, len));
    },
    _log_branch: function (parent, child) {
        log_branch(parent, child);
    }
});
