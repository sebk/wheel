use super::*;
use core::ptr;
use alloc::rc::Weak;
use alloc::boxed::Box;
use collections::{Vec, String};
use task::{PromiseCell};
use port::{PortRx, PortRxCell};

type Data = Vec<u8>;
type Handle = u32;

type DataTask = Task<(), Data>;
type HandleTask = Task<(), u32>;
type WriteTask = Task<Data, ()>;

type DataTaskRef = TaskRef<(), Data>;
type HandleTaskRef = TaskRef<(), Handle>;
type WriteTaskRef = TaskRef<Data, ()>;

#[derive(PartialEq, Eq, Debug)]
enum Tag {
    _Invalid,
    Data, // Vec<u8>
    Handle,
    Write
}

pub struct TaskRef<D, R> {
    weak:   Weak<PromiseCell<D, R>>,
    hub:    HubRef,
    tag:    Tag
}
fn _handle<D, R>(task: &Task<D, R>, tag: Tag) -> Box<TaskRef<D, R>> {
    Box::new(TaskRef {
        weak:   task.downgrade(),
        hub:    HubRef::cloned(),
        tag:    tag
    })
}
fn task_data(task: &Task<(), Data>) -> *mut DataTaskRef {
    Box::into_raw(_handle(task, Tag::Data))
}
fn task_write(task: &Task<Data, ()>) -> *mut WriteTaskRef {
    Box::into_raw(_handle(task, Tag::Write))
}
fn task_handle(task: &Task<(), Handle>) -> *mut HandleTaskRef {
    Box::into_raw(_handle(task, Tag::Handle))
}

#[allow(improper_ctypes)]
extern "C" {
    // transfers ownership to JS
    fn _async_fetch(
        task:       *mut DataTaskRef,
        url_ptr:    *const u8,
        url_len:    usize
    );
    
    fn _async_read(
        task:       *mut DataTaskRef,
        handle:     Handle
    );
    
    fn _async_write(
        task:       *mut WriteTaskRef,
        handle:     Handle,
        data_ptr:   *const u8,
        data_len:   usize
    );
    
    fn _async_open_directory(
        task:       *mut HandleTaskRef,
        name_ptr:   *const u8,
        name_len:   usize
    );
    
    fn _port_start(
        handle:     Handle,
        rx:         *mut Weak<PortRxCell>
    );
    
    fn _port_poll(
        handle:     Handle,
        data:       *mut Data
    ) -> bool;
    
    fn _port_send(
        handle:     Handle,
        data_ptr:   *const u8,
        data_len:   usize
    );
    
    fn _port_set_task(
        handle:     Handle,
        task:       *mut DataTaskRef
    );
    
    fn _dir_get_file(
        task:       *mut HandleTaskRef,
        handle:     Handle,
        name_ptr:   *const u8,
        name_len:   usize
    ) -> u32;
    
    fn _dir_get_directory(
        task:       *mut HandleTaskRef,
        handle:     u32,
        name_ptr:   *const u8,
        name_len:   usize
    ) -> u32;
        
    
    fn _log(id: usize, level: usize, ptr: *const u8, len: usize);
    fn _log_branch(parent: usize, child: usize);
}

pub fn fetch(url: &str) -> DataTask {
    let task = Task::future(());
    unsafe {
        _async_fetch(
            task_data(&task),
            url.as_ptr(),
            url.len()
        )
    };
    task
}

pub fn read(handle: Handle) -> DataTask {
    let task = Task::future(());
    unsafe {
        _async_read(
            task_data(&task),
            handle
        )
    };
    task
}

pub fn write(handle: Handle, data: Data) -> WriteTask {
    let data_ptr = data.as_ptr();
    let data_len = data.len();
    let task = Task::future(data);
    unsafe {
        _async_write(
            task_write(&task),
            handle,
            data_ptr,
            data_len
        )
    };
    task
}

pub fn async_open_directory(name: &str) -> HandleTask {
    let task = Task::future(());
    unsafe {
        _async_open_directory(
            task_handle(&task),
            name.as_ptr(),
            name.len()
        )
    };
    task
}

pub fn dir_get_file(handle: Handle, name: &str) -> HandleTask {
    let task = Task::future(());
    unsafe {
        _dir_get_file(
            task_handle(&task),
            handle,
            name.as_ptr(),
            name.len(),
        )
    };
    task
}

pub fn dir_get_directory(handle: Handle, name: &str) -> HandleTask {
    let task = Task::future(());
    unsafe {
        _dir_get_directory(
            task_handle(&task),
            handle,
            name.as_ptr(),
            name.len(),
        )
    };
    task
}

pub fn port_start(handle: Handle, rx: Box<Weak<PortRxCell>>) {
    unsafe {
        _port_start(
            handle,
            Box::into_raw(rx)
        )
    }
}
pub fn port_poll(handle: Handle) -> Option<Data> {
    let mut data = Data::new();
    let r = unsafe {
        _port_poll(
            handle,
            &mut data as *mut Data
        )
    };
    if r {
        Some(data)
    } else {
        None
    }
}

pub fn port_send(handle: Handle, data: &[u8]) {
    unsafe {
        _port_send(
            handle,
            data.as_ptr(),
            data.len()
        )
    }
}

pub fn log(id: usize, level: usize, s: &str) {
    unsafe {
        _log(id, level as usize, s.as_ptr(), s.len())
    }
}

pub fn log_branch(parent: usize, child: usize) {
    unsafe { _log_branch(parent, child); } 
}

#[no_mangle]
pub extern "C" fn task_complete_data(
    task_p:     *mut DataTaskRef,
    data_len:   usize
) -> *mut u8
{
    assert!(task_p != ptr::null_mut());
    let task = unsafe { &*task_p };
    assert_eq!(task.tag, Tag::Data);
    
    if let Some(rc) = task.weak.upgrade() {
        let mut data = Vec::with_capacity(data_len);
        unsafe {
            data.set_len(data_len);
        }
        let data_ptr = data.as_mut_slice().as_mut_ptr();
        rc.borrow_mut().complete(data);
        
        data_ptr
    } else {
        ptr::null_mut()
    }
}

#[no_mangle]
pub extern "C" fn task_failed_data(
    task_p:     *mut DataTaskRef,
    msg_len:    usize
) -> *mut u8 
{
    assert!(task_p != ptr::null_mut());
    let task = unsafe { &*task_p };
    assert_eq!(task.tag, Tag::Data);
    
    if let Some(rc) = task.weak.upgrade() {
        // warn!(task.log, "failed");
        let mut msg = String::with_capacity(msg_len);
        let msg_ptr = unsafe {
            let mut v = msg.as_mut_vec();
            v.set_len(msg_len);
            v.as_mut_slice().as_mut_ptr()
        };
        
        rc.borrow_mut().failed(msg);
    
        msg_ptr
    } else {
        ptr::null_mut()
    }
}

#[no_mangle]
pub extern "C" fn task_failed_handle(
    task_p:     *mut HandleTaskRef,
    msg_len:    usize
) -> *mut u8 
{
    assert!(task_p != ptr::null_mut());
    let task = unsafe { &*task_p };
    assert_eq!(task.tag, Tag::Handle);
    
    if let Some(rc) = task.weak.upgrade() {
        // warn!(task.log, "failed");
        let mut msg = String::with_capacity(msg_len);
        let msg_ptr = unsafe {
            let mut v = msg.as_mut_vec();
            v.set_len(msg_len);
            v.as_mut_slice().as_mut_ptr()
        };
        
        rc.borrow_mut().failed(msg);
    
        msg_ptr
    } else {
        ptr::null_mut()
    }
}

#[no_mangle]
pub extern "C" fn task_resume_complete_handle(task_p: *mut HandleTaskRef, data: Handle) {
    assert!(task_p != ptr::null_mut());
    let task = unsafe { Box::from_raw(task_p) };
    
    assert_eq!(task.tag, Tag::Handle);
    
    if let Some(rc) = task.weak.upgrade() {
        rc.borrow_mut().complete(data);
    }
    
    task.hub.resume();
}

#[no_mangle]
pub extern "C" fn task_resume_data(task_p: *mut DataTaskRef) {
    assert!(task_p != ptr::null_mut());
    let task = unsafe { Box::from_raw(task_p) };
    assert_eq!(task.tag, Tag::Data);
    task.hub.resume();
}
#[no_mangle]
pub extern "C" fn task_resume_failed_handle(task_p: *mut HandleTaskRef) {
    assert!(task_p != ptr::null_mut());
    let task = unsafe { Box::from_raw(task_p) };
    assert_eq!(task.tag, Tag::Handle);
    task.hub.resume();
}

#[no_mangle]
pub extern "C" fn set_data(data: *mut Data, len: usize) -> *mut u8 {
    assert!(data != ptr::null_mut());
    let data = unsafe { &mut *data };
    let cap = data.capacity();
    if len > cap {
        data.reserve(len - cap);
    }
    unsafe {
        data.set_len(len);
    }
    data.as_mut_slice().as_mut_ptr()
}

#[no_mangle]
pub extern "C" fn port_recv(
    port_rx_p:  *mut Weak<PortRxCell>,
    data_len:   usize
) -> *mut u8
{
    assert!(port_rx_p != ptr::null_mut());
    let port_rx = unsafe { &*port_rx_p };
    
    if let Some(rx) = port_rx.upgrade() {
        let state = &mut *rx.borrow_mut();
        match *state {
            PortRx::Pending => {
                let mut data = Vec::with_capacity(data_len);
                unsafe {
                    data.set_len(data_len);
                }
                let data_ptr = data.as_mut_slice().as_mut_ptr();
                *state = PortRx::Ready(data);
                
                data_ptr
            },
            _ => ptr::null_mut()
        }
    } else {
        ptr::null_mut()
    }
}

#[no_mangle]
pub extern "C" fn port_resume(port_rx_p: *mut Weak<PortRxCell>) {
    assert!(port_rx_p != ptr::null_mut());
    let _port_rx = unsafe { &*port_rx_p };
    HubRef::local(|hub| hub.resume());
}

pub mod export {
    pub use super::{
        task_resume_complete_handle,
        task_failed_handle,
        task_resume_failed_handle,
        task_complete_data,
        task_failed_data,
        task_resume_data,
        set_data,
        port_recv,
        port_resume
    };
}
