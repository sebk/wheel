let PORTS = new Array();
let PORTS_next = 1;
let HANDLES = new Array();
let HANDLES_next = 1;
let IMAGE_PORTS = new Array();
let PANICED = false;

const utf8Encoder = new TextEncoder();
const utf8Decoder = new TextDecoder();

// a usable fetch.
function read_from_url(url) {
    return fetch(url).then(function(r) {
        if (r.ok) {
            return r.arrayBuffer();
        } else {
            throw `failed to read from ${url}: ${r.status}`;
        }
    })
}

function utf8ToString(ptr, len) {
    return utf8Decoder.decode(Module.HEAPU8.subarray(ptr, ptr+len));
}

function stringToUtf8(string) {
    return utf8Encoder.encode(string);
}

function assert_type(x, type) {
    if (typeof(x) != type) {
        throw `wrong type: expected ${type}, found ${typeof(x)}.`;
    }
}

function assert_ptr(x) {
    assert_type(x, "number");
    if (parseInt(x) != x || x <= 0) {
        throw "not a pointer!";
    }
}

let OPEN_DIRECTORIES = new Array();
function open_directory(name) {
    assert_type(name, "string");
    let handle = OPEN_DIRECTORIES[name];
    if (handle == undefined) {
        handle = add_handle(Directory(name));
    }
    return Promise.resolve(handle);
}

function Directory(base) {
    return {
        get_file: function(name) {
            assert_type(name, "string");
            return Promise.resolve(add_url(`${base}/${name}`));
        },
        get_directory: function(name) {
            assert_type(name, "string");
            let handle = add_handle(Directory(`${base}/${name}`));
            return Promise.resolve(handle);
        }
    };
}

function mount(webfs) {
    return Directory(webfs.root);
}

function dirname(p) {
    let i = p.lastIndexOf('/');
    return p.substring(0, i);
}

function search(handles, name) {
    let promises = [];
    for (let h of handles) {
        promises.push(mountpoints[h].read(name));
    }
    return Promise.race(promises);
}

function read_file(handle) {
    return new Promise(function (resolve, reject) {
        let reader = new FileReader();
        reader.onload = function() { resolve(reader.result) };
        reader.onerror = function(e) { reject(e.type) };
        reader.readAsArrayBuffer(HANDLES[handle]);
    });
}

function add_handle(b) {
    let h = HANDLES_next;
    HANDLES_next += 1;
    HANDLES[h] = b;
    return h;
}
function add_url(url) {
    return add_handle({
        read: function() {
            return read_from_url(url);
        }
    });
}
const LOG_NAMES = {
    1:  "Trace",
    2:  "Debug",
    3:  "Info",
    4:  "Warn",
    5:  "Error"
};
function log(id, level, msg) {
    //let f = log_functions[level];
    //f(id, msg);
    console.log(id, msg);
}
function log_branch(parent, child) {}

function DataTask(_task) {
    assert_ptr(_task);
    
    return {
        complete: function(data) {
            if (PANICED || _task === null) return;
            var task = _task;
            _task = null;
            
            
            let arr = new Uint8Array(data);
            try {
                var buf_ptr = Module._task_complete_data(task, arr.byteLength);
                if (buf_ptr !== 0) {
                    Module.HEAPU8.set(arr, buf_ptr);
                }
                Module._task_resume_data(task);
            } catch(e) {
                PANICED = true;
                throw e;
            }
        },
    
        failed: function(msg) {
            assert_type(msg, "string");
            
            if (PANICED || _task === null) return;
            var task = _task;
            _task = null;
            
            var msg_utf8 = stringToUtf8(msg);
            var utf8_len = msg_utf8.length;
            try {
                var buf_ptr = Module._task_failed_data(task, msg_utf8.length);
                if (buf_ptr !== 0) {
                    Module.HEAPU8.set(msg_utf8, buf_ptr);
                }
                Module._task_resume_data(task);
            } catch(e) {
                PANICED = true;
                throw e;
            }
        }
    };
};
function HandleTask(_task) {
    assert_ptr(_task);
    
    return {
        complete: function(handle) {
            if (PANICED || _task === null) return;
            var task = _task;
            _task = null;
            
            try {
                Module._task_resume_complete_handle(task, handle);
            } catch(e) {
                PANICED = true;
                throw e;
            }
        },
    
        failed: function(msg) {
            if (PANICED || _task === null) return;
            var task = _task;
            _task = null;
            
            var msg_utf8 = stringToUtf8(msg);
            var utf8_len = msg_utf8.length;
            try {
                var buf_ptr = Module._task_failed_handle(task, msg_utf8.length);
                if (buf_ptr !== 0) {
                    Module.HEAPU8.set(msg_utf8, buf_ptr);
                }
                Module._task_resume_failed_handle(task);
            } catch(e) {
                PANICED = true;
                throw e;
            }
        }
    };
};

function port_register(p) {
    PORTS[++PORTS_next] = p
    return PORTS_next;
}

function Port(send_raw) {
    var port_ptr = null;
    var rx_queue = [];
    
    // called from Rust
    function send(data) {
        var msg;
        switch (data.type) {
            case "image":
                var img = new ImageData(new Uint8ClampedArray(data.data), data.width, data.height);
                msg = { type: "image", value: img };
                break;
            default:
                msg = data;
                break;
        }
        send_raw(msg);
    }
    // called form channel callback
    function recv(data) {
        rx_queue.push(data);
    }
    
    // push one element into the Port
    function recv_data(ptr) {
        try {
            var buf_ptr = Module._port_recv(port_ptr, arr.byteLength);
            if (buf_ptr !== 0) {
                Module.HEAPU8.set(arr, buf_ptr);
                Module._port_resume(port_ptr);
            } else {
                rx_queue.push(arr);
            }
        } catch(e) {
            PANICED = true;
            throw e;
        }
    }
    
    // Rust calls this once the port is initialized
    function start(ptr) {
        port_ptr = ptr;
        if (rx_queue.length) {
            recv_data(rx_queue.shift())
        }
    }
    
    function poll(data) {
        if (rx_queue.length) {
            var arr = rx_queue.shift();
            try {
                var buf_ptr = Module._set_data(data, arr.byteLength);
                if (buf_ptr !== 0) {
                    Module.HEAPU8.set(arr, buf_ptr);
                }
            } catch(e) {
                PANICED = true;
                throw e;
            }
            return true;
        } else {
            return false;
        }
    }
    
    return {
        send:  send,
        poll:  poll,
        start: start,
        recv:  recv
    };
}

function channel_port(c) {
    var port = Port(function (data) {
        c.postMessage();
    });
    c.onmessage = function (e) {
        port.recv(e.data);
    };
    return port_register(port);
}

var Module = {
    preInit: function() {
        let abort = Module["abort"];
        Module["abort"] = function() {
            PANICED = true;
            abort();
        };
    }
};
