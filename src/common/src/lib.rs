#![no_std]
#![feature(alloc)]

extern crate alloc;
extern crate futures;

use core::fmt::{Debug, Arguments};
use alloc::boxed::Box;
use futures::Future;


pub trait Log: Sized {
    fn root() -> Self;
    fn branch(&self) -> Self;
    fn log(&self, level: LogLevel, args: Arguments);
}

pub trait IoUrl: Sized {
    type Buffer;
    type Error: Debug;

    fn read_url(url: &str) -> Box<Future<Item=Self::Buffer, Error=Self::Error>>;
}

pub trait Io: Sized {
    type File: AsyncOpen + AsyncRead;
    type Directory: AsyncOpen + AsyncDirectory;
    type Buffer;
}

pub trait AsyncOpen: Sized {
    type Error: Debug;
    fn open(name: &str) -> Box<Future<Item=Self, Error=Self::Error>>;
}

pub trait AsyncDirectory: Sized {
    type File: AsyncRead;
    type Error: Debug;
    
    fn get_directory(&self, name: &str) -> Box<Future<Item=Self, Error=Self::Error>>;
    fn get_file(&self, name: &str) -> Box<Future<Item=Self::File, Error=Self::Error>>;
}
pub trait AsyncRead: Sized {
    type Error;
    type Buffer;
    
    fn read(&self) -> Box<Future<Item=Self::Buffer, Error=Self::Error>>;
}
pub trait AsyncWrite: Sized {
    type Error;
    type Buffer;
    
    fn write(&self, data: Self::Buffer) -> Box<Future<Item=Self::Buffer, Error=Self::Error>>;
}

pub enum LogLevel {
    Trace = 1,
    Debug = 2,
    Info = 3,
    Warn = 4,
    Error = 5,
}
