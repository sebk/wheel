#![feature(box_syntax)]

extern crate common;

#[cfg(any(target_arch = "asmjs", target_arch = "wasm32"))]
pub extern crate asmjs;

#[cfg(target_os = "linux")]
pub extern crate linux;

extern crate futures;

// Glue

#[cfg(any(target_arch = "asmjs", target_arch = "wasm32"))]
pub use asmjs::{Log, Hub, HubRef, Port, export, File, Directory};

#[cfg(target_os = "linux")]
pub use linux::{Log, Hub, HubRef, SharedFile as File, SharedDirectory as Directory};


pub use common::{AsyncRead};

use futures::{Future, Stream};
use std::fmt::Debug;

#[macro_use]
pub mod log;


pub fn run<F, Item, Error>(f: F) where
    F: Future<Item=Item, Error=Error> + 'static,
    Item: Debug,
    Error: Debug
{
    HubRef::local(|hub| hub
        .execute_future(
            box f
            .map(|item| {
                debug!(Log::root(), "future result: {:?}", item)
            })
            .map_err(|err| error!(Log::root(), "future error: {:?}", err))
        )
    )
}
pub fn run_forever<S, Item, Error>(s: S) where
    S: Stream<Item=Item, Error=Error> + 'static,
    Item: Debug,
    Error: Debug
{
    HubRef::local(|hub| hub
        .execute_stream(
            box s
            .map(|item| {
                debug!(Log::root(), "stream result: {:?}", item)
            })
            .map_err(|err| error!(Log::root(), "stream error: {:?}", err))
        )
    )
}

pub mod prelude {
    pub use common::{AsyncRead, AsyncWrite, AsyncDirectory, AsyncOpen};
    pub use super::{File, Directory, Log};
}

/*
pub fn read_url(url: &str) -> ReadFuture {
    Hub::local().read_url(url)
}


pub fn search_file<I>(name: &str, folders: I) -> ReadFuture where
I: Iterator<Item=Directory>
{
    HUB.search(name, folders.map(|d| d.handle))
}
*/
