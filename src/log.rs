pub use common::LogLevel;

#[macro_export]
macro_rules! log {
    ($log:expr, $level:expr, $($arg:tt)+) => ({
        $log.log($level, format_args!($($arg)+))
    })
}

#[macro_export]
macro_rules! error {
    ($log:expr, $($arg:tt)+) => ({
        $log.log($crate::log::LogLevel::Error, format_args!($($arg)+))
    })
}

#[macro_export]
macro_rules! warn {
    ($log:expr, $($arg:tt)+) => ({
        $log.log($crate::log::LogLevel::Warn, format_args!($($arg)+))
    })
}

#[macro_export]
macro_rules! info {
    ($log:expr, $($arg:tt)+) => ({
        $log.log($crate::log::LogLevel::Info, format_args!($($arg)+))
    })
}

#[macro_export]
macro_rules! debug {
    ($log:expr, $($arg:tt)+) => ({
        $log.log($crate::log::LogLevel::Debug, format_args!($($arg)+))
    })
}

#[macro_export]
macro_rules! trace {
    ($log:expr, $($arg:tt)+) => ({
        $log.log($crate::log::LogLevel::Trace, format_args!($($arg)+))
    })
}

